package com.task5810.jpahibernate.controller;
import com.task5810.jpahibernate.model.CVoucher;
import com.task5810.jpahibernate.repository.IVoucherRepository;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class CVoucherController {
    @Autowired
    IVoucherRepository voucherRepository;
    
    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getVouchers() {
        try {
            List<CVoucher> lstVoucher = new ArrayList<CVoucher>();
            voucherRepository.findAll().forEach(lstVoucher::add);

            if (lstVoucher.size() == 0) {
                return new ResponseEntity<List<CVoucher>>(lstVoucher, HttpStatus.NOT_FOUND);                
            } else {
                return new ResponseEntity<List<CVoucher>>(lstVoucher, HttpStatus.OK);                
            }
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);                
        }
    }

    @GetMapping("/vouchers1")
    public List<CVoucher> getVouchers1() {
        List<CVoucher> lstVoucher = new ArrayList<CVoucher>();
        try {           
            voucherRepository.findAll().forEach(lstVoucher::add);
        } catch (Exception e) {
            //TODO: handle exception
        }

        return lstVoucher;
    }    
}
